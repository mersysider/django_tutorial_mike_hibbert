from tastypie.resources import ModelResource

'''
in web services things are called resources, a paradigm,
we r gonna use our Article model, and turn that into an resource or a web service taht you can then query 
for articles inside of the databse through the django frameworks url

''' 
from tastypie.constants import ALL
'''
allows us to set the typoes of query types that we can form in our models 
for eg. we can list all the articles iun the article collection
or filter
'''

from models import Article

class ArticleResource(ModelResource):
	class Meta:
		queryset = Article.objects.all()
		resource_name = 'article'

		'''
		when this web serives is called in our url, its name will be called article
		eg: localhost:8000/articles/api/article
		'''
		filtering = {'title' : ALL}
		# says the fields inside of this
		# what kind of filtering would we allow to be performed throu the web service
		# ... on this particula part of the table(title part)
		# means the title can be filtered in all number of ways 
		# filtering = {'title' : "contains" }
		# title__contains

