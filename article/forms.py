from django import forms
from .models import Article, Comment
# from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm 
# from django.core.exceptions import ValidationError

class ArticleForm(forms.ModelForm):
	# ModelForm is a form made to deal with models specifically

	class Meta:
		# this adds an extra bit of information
		# define anything that isnt actually a form field
		model = Article
		fields = ('title','body','pub_date','thumbnail')

	
class CommentForm(forms.ModelForm):

	class Meta:
		model = Comment
		fields = ('name', 'body')