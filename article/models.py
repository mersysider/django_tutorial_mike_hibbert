from __future__ import unicode_literals
from django.db import models
import datetime
from time import time
from django_test.settings import UPLOAD_FILE_PATTERN, DEBUG

# for the thumbnail upload
def get_upload_file_name(instance, filename):
	# return "uploaded_files/{}_{}".format(str(time()).replace('.','_'), filename)
	return UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.
class Article(models.Model):
	# 200 characters
	title = models.CharField(max_length=200)
	body = models.TextField()
	pub_date = models.DateTimeField('date published', blank=True, null=True)
	'''
	 # default=datetime.date.today(),
	 # when i do default above, the create articles website works fine, 
	 # however, the admin create admin doesnt work
	 '''
	likes = models.IntegerField(default=0) 

	# ADD THUMBNAIL, used tastypie to migrate
	thumbnail = models.FileField(upload_to=get_upload_file_name, blank=True, null=True)
	# get_upload_file_name is a function that you create, because you can perform some logic like account authorization


	def __unicode__(self):
		return self.title
	
	# for testing views
	def get_absolute_url(self):
		return "/articles/get/%i/" %self.id
		# it returns a string that tells us what we consider a proper url for an article could be

	def get_thumbnail(self):
		thumb = str(self.thumbnail)
		if not DEBUG:
			thumb = thumb.replace('assets/','')
		return thumb

class Comment(models.Model):

	name = models.CharField(max_length=200)
	# first_name = models.CharField(max_length=200)
	# second_name = models.CharField(max_length=200)

	body = models.TextField()
	pub_date = models.DateTimeField('date published')
	# established relationship
	# for more google django models 
	article = models.ForeignKey(Article)
	# this means article sis relatred to Article class

	def __unicode__(self):
		return self.name