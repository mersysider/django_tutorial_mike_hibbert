# template tag functions should always have a value and an argument
from django import template

# to register template tags
register = template.Library()

# the filter name is what the function will be known by when we use it in opur template html file
@register.filter(name='article_shorten_body')
def article_shorten_body(bodytext, length):
	# bodytext is value obj and length is the argument

	if len(bodytext) > length:
		text = "%s ..." %bodytext[1:length]
	else: 
		text = bodytext
	return text