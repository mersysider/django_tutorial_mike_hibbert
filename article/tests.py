from django.test import TestCase

# Create your tests here.

from article.models import Article, get_upload_file_name
# because the function, get_upload_file_name: is inside the Article model
from django.utils import timezone
from time import time

# testing for the views
from django.core.urlresolvers import reverse
# the resolve function can resolve 
# ...the view mapping through in terms of what it should be interms of browser url
#  for eg. the views we've written in python, 
# ...reverse knows how to turn that view (article.views.articles) into a url 
from article.views import articles, article


class ArticleTest(TestCase):
	def create_article(self, title="test article", body="Blah Blah bleh"):
		return Article.objects.create(title=title,
			body=body,
			pub_date=timezone.now(),
			likes=0)

	def test_article_creation(self):
	# tests that we can actually create an article
		a = self.create_article()

		self.assertTrue(isinstance(a, Article))
		# assertTrue comes along with TestCase class, 
		# also assertEqual, assertIn (string inside another string)
		#  tests can you create an article using an Article model

		self.assertEqual(a.__unicode__(), a.title )
		#  is the unicode function working?

	def test_get_upload_file_name(self):
		filename = "Cheese.txt"
		path = "uploaded_files/{}_{}".format(str(time()).replace('.','_'), filename)
		created_path = get_upload_file_name(self, filename)
		#using path what we think it should be PATH and 
		# and we generate a path what the function thinks it should be, CREATED_PATH
		self.assertEqual(path, created_path)
				#  and we do a test if they are equal and if they arents the tets will fail

		
# VIEW tests
	def test_articles_list_view(self):
		# list view is the list page with all the articles listed one by one

		# create and article and user reverse
		a = self.create_article()
		url = reverse(articles)
		resp = self.client.get(url)
		# TestCase class has a built in web client, 
		# ...that can go and visit urls as if it is a browser, 
		# ...and return what the response from that url would be

		self.assertEqual(resp.status_code, 200)
		self.assertIn(a.title, resp.content)

	# test individual article
	def test_article_detail_view(self):
		a = self.create_article()
		url = reverse(article, args=[a.id])
		resp = self.client.get(url)

		self.assertEqual(reverse(article, args=[a.id]),a.get_absolute_url())
		self.assertEqual(resp.status_code, 200)
		self.assertIn(a.title, resp.content)