from django.conf.urls import patterns, include, url
from .views import (articles, article, language, create, like_article,
 					add_comment, search_titles)
from .api import ArticleResource

# creating an instance
article_resource = ArticleResource()

urlpatterns =[
	url(r'^all/$', articles),
	url(r'^get/(?P<article_id>\d+)/$',article),
	url(r'^language/(?P<language>[a-z\-]+)/$',language),
	url(r'^create/$',create),
	url(r'^like/(?P<article_id>\d+)/$',like_article),
	url(r'^add_comment/(?P<article_id>\d+)/$',add_comment),
	url(r'^search/$', search_titles),
	url(r'^api/', include(article_resource.urls)),
	]

