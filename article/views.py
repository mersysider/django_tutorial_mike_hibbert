from django.shortcuts import render_to_response
from article.models import Article, Comment
from django.http import HttpResponse, HttpResponseRedirect
from .forms import ArticleForm, CommentForm
from django.template.context_processors import csrf
from django.utils import timezone
# Create your views here.

def articles(request):
	''' EVERY VIEW FUNCTION NEEDS A REQUEST 
	sends all articles to the home page, articles.html'''


	language = "en-us"
	session_language = "en-us"

	if "lang" in request.COOKIES:
		language = request.COOKIES["lang"]

	if "lang" in request.session:
		session_language = request.session["lang"]

	args = {}
	args.update(csrf(request))
	# this was added for ajax call

	args['articles'] = Article.objects.all()
	args['language'] = language
	args['session_language'] = session_language 

	return render_to_response('articles.html',args)

def article(request, article_id=1):
	'''sends the requested (GET) article to a different page'''
	return render_to_response('article.html',{'article': Article.objects.get(id=article_id)})


def language(request, language="en-us"):
	response = HttpResponse("setting language to %s" %language)
	response.set_cookie("lang", language)
	request.session["lang"] = language
	return response

def create(request):
	if request.POST:
		# prevents hacking from url(GET method)
		form = ArticleForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/articles/all')
	else:
		form = ArticleForm()

	args = {}
	args.update(csrf(request))
	args['form'] = form
	return render_to_response('create_article.html',args)

def like_article(request, article_id=1):
	'''UPDATING THE DATABASE
	this is for liking the article'''
	if article_id:
		a = Article.objects.get(id=article_id)
		count = a.likes
		count += 1
		a.likes = count
		a.save()

	return HttpResponseRedirect('/articles/get/%s' %article_id)

def add_comment(request, article_id=1):
	# a is the article object
	a = Article.objects.get(id=article_id)
	# post it to the corresponding article
	if request.method=="POST":
		# f is the comment form
		f = CommentForm(request.POST)
		if f.is_valid():
			# to establish the rel b/w comment and the article make and instance "C" but dont save it yet
			c = f.save(commit=False)
			# cis the comment object
			c.pub_date = timezone.now()
			c.article = a
			c.save()
			# now the rel is established and saved in c object
			return HttpResponseRedirect('/articles/get/%s' %article_id)
	else:
		# send nothing to CommentForm modelForm
		f = CommentForm()

	# to have our form protected add CSRF
	args = {}
	args.update(csrf(request))

	# what do you wanna pass through to the template?
	# article and form
	args['article'] = a
	args['form'] = f

	return render_to_response('add_comment.html', args)

def search_titles(request):
	if request.method =="POST":
		search_text = request.POST['search_text']
	else:
		search_text = ''

	articles= Article.objects.filter(title__contains=search_text)
	return render_to_response('ajax_search.html',
		{'articles':articles})