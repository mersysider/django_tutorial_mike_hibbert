from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm 
# from django.core.exceptions import ValidationError

# not a modelForm
class MyRegistrationForm(UserCreationForm):
	email = forms.EmailField(required=True, help_text="Please enter your email")

	class Meta:
		# this adds an extra bit of information
		# define anything that isnt actually a form field
		model = User
		fields = ('username','email','password1', 'password2')

	
	# updating database
	def save(self, commit=True):
		user = super(UserCreationForm, self).save(commit=False)
		# thr super command calls the base class
		# not save it yet
		user.email = self.cleaned_data['email']
		if commit:
			user.save()
			# this saves the user
		return user

	# def clean_username(self):
	# 	username = self.cleaned_data['username']
	# 	if User.objects.filter(username=username).exists():
	# 		raise ValidationError("User with this username already exists")

	# 	return username

	# def clean_password2(self):
	# 	password1 = self.cleaned_data['password1']
	# 	password2 = self.cleaned_data['password2'] 


	# 	if password1 and password2 and password1  != password2:
	# 		raise ValidationError("Your two passwords did not match. Please enter again")

	# 	return password2

	# username = forms.CharField(max_length=30)
	# password1 = forms.CharField(max_length=30, help_text="Please enter your password", widget=forms.PasswordInput)
	# password2 = forms.CharField(max_length=30, help_text="Please enter the same password for verification", widget=forms.PasswordInput)
	# organisation = forms.BooleanField(label="Are you an organisation ?", initial=True, required=False)

# DJANGO FORM WIZARD

class ContactForm1(forms.Form):
	subject = forms.CharField(max_length=100)


class ContactForm2(forms.Form):
	sender = forms.EmailField()


class ContactForm3(forms.Form):
	message = forms.CharField(widget=forms.Textarea)