from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
# allows the browser to redirect to different url
from django.contrib import auth 
from django.template.context_processors import csrf
# from django.contrib.auth.forms import UserCreationForm 
from forms import MyRegistrationForm, ContactForm1, ContactForm2, ContactForm3
from formtools.wizard.views import SessionWizardView
# or CookieWizardView
from django.core.mail import send_mail

# once weve complete evrything we should get back the data and to check all the data, 
# OLD DJANGO
# import logging
# logr = logging.getLogger(__name__)

def login(request):
	c = {}
	c.update(csrf(request))
	return render_to_response('login.html',c)

def auth_view(request):
	username = request.POST.get('username','')
	#if we dont get a value for usernmae, we get an empty variable so it doesnt break
	# if you find a value return ""
	password = request.POST.get('password','')
	user = auth.authenticate(username=username, password=password)
	# if a match, return user object
	# if no mathces returns None 
	if user is not None:
		auth.login(request, user)
		return HttpResponseRedirect('/accounts/loggedin')
	else:
		return HttpResponseRedirect('/accounts/invalid')

def loggedin(request):
	return render_to_response('loggedin.html',{'full_name': request.user.username})


def invalid_login(request):
	return render_to_response('invalid_login.html')


def logout(request):
	auth.logout(request)
	return render_to_response('logout.html')

def register_user(request):
	if request.method == "POST":
		# form = UserCreationForm(request.POST)
		form = MyRegistrationForm(request.POST)
		if form.is_valid():
			# validates
			form.save()
			print "===================WORKED========================"
			return HttpResponseRedirect('/accounts/register_success')
	
	args = {}
	args.update(csrf(request))

	# args['form'] = UserCreationForm()
	args['form'] = MyRegistrationForm()

	# a blank user creation form, has NO INFO put in it, NO DATA
	return render_to_response('register.html', args)

def register_success(request):
	return render_to_response('register_success.html')

# for forms that are spread in different pages
class ContactWizard(SessionWizardView):
	template_name = "contact_form.html"
	# afte ALL data is taken across various forms
	def done(self, form_list, **kwargs):
		'''
			arguments: self, cuz its a class
						form_list, a list of forms
						and key word arguments: just in case if theere s anything else
		'''
		# to process the form data using a function
		form_data = process_form_data(form_list)
		
		return render_to_response('done.html',{'form_data':form_data })
 # to email the final data
def process_form_data(form_list):
# this is alist with all the clean info from the forms
	form_data = [form.cleaned_data for form in form_list]
	print "FORM DATA--------------------------------------"
	print form_data[1]['sender']

	# # three login commands
	# logr.debug(form_data[0]['subject'])
	# logr.debug(form_data[1]['sender'])
	# logr.debug(form_data[2]['message'])

	# # use the send mail function
	# send_mail(form_data[0]['subject'],
	# 	form_data[2]['message'],
	# 	form_data[1]['sender'],
	# 	['mersysider@gmail.com'],
	# 	fail_silently=False)
	return form_data			