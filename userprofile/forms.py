from .models import UserProfile
from django import forms

class UserProfileForm(forms.ModelForm):
	
	class Meta:
		model = UserProfile
		# fields = '__all__'
		# no user for security purposes
		# should be loggedin
		fields = ('likes_cheese','favorite_hamster_name')