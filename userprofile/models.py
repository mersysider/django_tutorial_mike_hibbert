from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class UserProfile(models.Model):
	 # to tie the relationship w. User model
	user = models.OneToOneField(User)
	likes_cheese = models.BooleanField(default=True)
	favorite_hamster_name = models.CharField(max_length=50)

# the User has a (profile) member variable/property
# the profile is a property and whenever we are passing an user object to this property, (u), 
# it whill shoot off an (UserProfile) model 
# it should return a USerProfile object
User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])