from django.conf.urls import patterns, url, include

from .views import user_profile

urlpatterns = [ 
	url(r'^profile/$', user_profile, name="user-profile"),
]