from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template.context_processors import csrf 
from .forms import UserProfileForm
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def user_profile(request):
	if request.method =="POST":
		# the reason for instance, were saying we want to populate the form with the original instance
		# ...of the profile
		form = UserProfileForm(request.POST, instance=request.user.profile)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/accounts/loggedin')
	else:
		# send the info of the loggedin user
		user = request.user

		# this triggers that code in models
		# User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
		#  this will create the user progile, populate and push it back to tyhe variable
		profile = user.profile
		form = UserProfileForm(instance=profile)

	args = {}
	args.update(csrf(request))
	args['form'] = form

	return render_to_response('profile.html',args) 